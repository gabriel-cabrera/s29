// for # 2

db.users.find( 
	{ $or: [ {firstName: {$regex: 's', $options: '$i'}}, {lastName: {$regex: 'd', $options: '$i'}} ]}, 
	{
		firstName: true,
		lastName: true,
		_id: false
});

// for # 3

db.users.find( { $and: [ { department: "HR"}, { age: { $gte: 70}}]});

// for # 4

db.users.find( 
	{ 
		$and: [ {firstName: {$regex: 'e', $options: '$i'} },
	 { age : { $lte: 30}}] 
	}
);